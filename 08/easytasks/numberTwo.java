package easytasks;

import java.util.Scanner;

public class numberTwo {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    System.out.println("Введите число: ");
    double number = sc.nextDouble();
    double res = 1;

    for (int i=1; i<=number; i++) {
      res *= i;
    }
    System.out.println(res);
  }
}
