package easytasks;

import java.util.Scanner;

public class numberFour {
  public static void main(String[] args) {

    Scanner sc = new Scanner(System.in);
    System.out.println("Введите желаемый размер матрицы: ");
    int width = sc.nextInt();
    int height = sc.nextInt();

    int[][] field;
    field  = new int[width][height];

    for (int i=0; i<width; i++) {
      System.out.println("Введите " + height + "чисел!");
      for (int j=0; j<height; j++) {
        field[i][j] = sc.nextInt();
      }
    }

    System.out.println("Вывод транспонированной матрицы: ");

    for (int i=0; i<height; i++) {
      for (int j=0; j<width; j++) {
        System.out.printf("%d ", field[j][i]);
        System.out.print(" ");
      }

      System.out.println(" ");
    }

  }
}
