package easytasks;

import java.util.Scanner;

class numberSix {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    System.out.print("Введите желаемый размер матрицы: ");
    int number = sc.nextInt();
    int[][] field;
    field = new int[number][number];
    for (int i=0; i<number; i++) {
      System.out.println("Введите " + number + " чисел!");
      for (int j=0; j < number; j++) {
        field[i][j] = sc.nextInt();
      }
    }
    System.out.println("Числа ниже главной диагонали: ");
    for (int i=0; i<number; i++) {
      for (int j=0; j<number; j++) {
        if (i>j) {
          System.out.printf("%d", field[i][j]);
          System.out.print(' ');
        }
        else {
          System.out.println(" ");
        }
      }
      System.out.println(" ");
    }
  }
}
