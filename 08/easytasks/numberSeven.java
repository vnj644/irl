

import java.util.Scanner;

class numberSeven {
  public static void main(String args[]) {
    Scanner sc = new Scanner(System.in);

    System.out.println("Введите желаемый размер матрица: ");

    int width = sc.nextInt();
    int height = sc.nextInt();

    int[][] field;
    field = new int[width][height];
    for (int i=0; i<width; i++) {
      System.out.println("Введите " + width + " чисел: ");
      for (int j=0; j<height; j++) {
        field[i][j] = sc.nextInt();
      }
    }
    System.out.println("Матрица в одну строку: ");
    for (int i=0; i<width; i++) {
      for (int j=0; j<height; j++) {
        System.out.printf("%d ", field[i][j]);
        System.out.print(' ');
      }
    }
  }
}
