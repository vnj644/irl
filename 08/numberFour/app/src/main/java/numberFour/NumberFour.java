package numberFour;

import java.util.Date;
import java.util.Scanner;
import java.util.Random;

public class NumberFour {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    System.out.println("Enter max num: ");
    double maxNum = sc.nextDouble();
    System.out.println("Enter a coef: ");
    double coef = sc.nextDouble();
    System.out.println("Enter a max time: ");
    double maxTime = sc.nextLong();
    Thread thr = new Thread();
    double sum = Math.random()*(2*(maxNum*coef)) - (maxNum*coef);
    long startTime = System.currentTimeMillis();
    long endTime = startTime;
    boolean tr = true;
    while (tr) {
      if (sum <= maxNum && ((endTime - startTime) <= maxTime)) {
        try{
          thr.sleep(new Random().nextInt(10));
        }
        catch (Exception e){}
        endTime = System.currentTimeMillis();
      }
      else {
        tr = false;
      }

      long razn = endTime - startTime;
      System.out.println(sum + " " + (razn));
    }
  }
}
