package numberSix;

public class Operations {
  public static int[][] multiply(int[][] array, int[][] secArray) throws NullPointerException {
    if (array == null || secArray == null) {
      throw new NullPointerException();
    }
    int[][] thrArray = new int[array.length][secArray.length];
    int sum = 0;
    for (int i=0; i<array.length; i++) {
      for (int j=0; j<secArray[i].length; j++) {
        for (int y=0; y<secArray[j].length; y++) {
          sum+=array[y][j];
        }
        thrArray[i][j] = sum;
      }
    }
    return thrArray;
  }
  public static void printMatrix(int[][] array, int size) throws NullPointerException {
    if (array == null) {
      throw new NullPointerException();
    }
    for (int i=0; i<size; i++) {
      for (int j=0; j<size; j++) {
        System.out.print(array[i][j] + " ");
      }
      System.out.println();
    }
    System.out.println();
  }
}
