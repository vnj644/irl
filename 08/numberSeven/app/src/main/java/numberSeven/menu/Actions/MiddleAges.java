package menu.Actions;

public class MiddleAges extends ActionPat {
  String description = "Выберите эпоху развития: ";
  public MiddleAges(String description) {
    super(description);
  }
  public void act() {
    System.out.println("История средних веков затрагивает период с V по XVI век. Концом европейского средневековья считается начало Нидерландской буржуазной революции (1566 г.).");
  }
}
