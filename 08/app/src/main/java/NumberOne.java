import java.util.Scanner;

package java;

public class NumberOne {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    System.out.print("Введите число: ");
    try {
      int number = sc.nextInt();
      System.out.println("Вы ввели число: " + number);
    }
    catch(java.util.InputMismatchException e) {
      System.out.println("Ошибка. Строка может состоять только из цифр!");
    }
  }
}
