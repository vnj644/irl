package numberOne;

public class NewException extends Exception {
  NewException(String printer) {
    super(printer);
  }
}
