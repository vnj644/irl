package menu;

public class Action {
  String description;
  String message;

  public Action(String description) {
    this.message = message;
  }

  public void act() {
    System.out.println(this.message);
  }

  public String getDescription() {
    return this.description;
  }

  public void setMessage(String message) {
    this.message = message;
  }
}
