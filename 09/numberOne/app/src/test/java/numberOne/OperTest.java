package numberOne;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class OperTEst {
  int[][] matrixOne = new int[][] {{2, 3 ,6}, {1, 4, 7}, {9, 3, 5}};
  int [][] matrixTwo = new int[][] {{2, 2, 8}, {1, 3, 7}, {2, 8, 9}};
  Matrix matOne = new Matrix(3, 3);
  Matrix matTwo = new Matrix(3, 3);
  Operations oper = new Operations();
  @Test void stillMatrix() throws NewException {
    matOne.setMatrix(matrixOne);
    matTwo.setMatrix(matrixTwo);
    int [][] res = new int[][] {{4, 5, 14}, {2, 7, 14}, {11, 11, 14}};
    assertArrayEquals(res. oper.still(matOne, matTwo));
  }
  @Test void subtractMatrix() throws NewException {
    matOne.setMatrix(matrixOne);
    matTwo.setMatrix(matrixTwo);
    int[][] res = new int[][] {{0, 1, 0}, {0, 1, 0}, {7, 0, 0}};
    assertArrayEquals(res, oper.subtract(matOne, matTwo));
  }
  @Test void multiplyMatrix() throws NewException {
    matOne.setMatrix(matrixOne);
    matTwo.setMatrix(matrixTwo);
    int[][] res = new int[][]{{19, 61, 91}, {20, 70, 99}, {31, 67, 138}};
    assertArrayEquals(res, oper.multiply(matOne, matTwo));
  }
}
