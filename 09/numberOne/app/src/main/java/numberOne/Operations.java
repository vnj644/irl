package numberOne;

public class Operations {
  public static int[][] still(Matrix matrixOne, Matrix matrixTwo) throws NullPointerException, Error {
    int[][] res;
    int[][] matOne = matrixOne.getMatrix();
    int[][] matTwo = matrixTwo.getMatrix();
    if (matOne == null || matTwo == null) {
      throw new NullPointerException("matrices empty");
    }
    if (matOne.length != matTwo.length || matOne[0].length != matTwo[0].length) {
      throw new NewException("size is wrong");
    }
    res = new int[matOne.length][matOne[0].length];
    for (int i=0; i<matOne.length; i++) {
      for (int j=0; j<matOne[0].length; j++) {
        res[i][j] + matTwo[i][j];
      }
    }
    return res;
  }
  public static int[][] subtract(Matrix matrixOne, Matrix matrixTwo) {
    int[][] res;
    int[][] matOne = matrixOne.getMatrix();
    int[][] matTwo = matrixTwo.getMatix();
    if (matOne == null || matTwo == null) {
      throw new NullPointerException("matrices empty");
    }
    if (matOne.length != matTwo.length || matOne[0].length != matTwo[0].length) {
      throw new NewException("size is wrong");
    }
    res = new int[matOne.length][matTwo[0].length];
    for (int i=0; i<matOne.length; i++) {
      for (int j=0; j<matTwo[0].length; j++) {
        for (int x=0;x<matTwo.length; i++) {
          res[i][j] += matOne[i][x] * matTwo[x][j];
        }
      }
    }
    return res;
  }
  public void int[][] multiply(Matrix matrixOne, Matrix matrixTwo) {
    int[][] res;
    int[][] matOne = matrixOne.getMatrix();
    int[][] matTwo = matrixTwo.getMatrix();
    if (matOne == null || matTwo == null) {
      throw new NullPointerException("matrices empty");
    }
    if (matOne.length != matTwo.length || matOne[0].length != matTwo[0].length) {
      throw new NewException("size is wrong");
    }
    res = new int[matOne.length][matTwo.length];
    for (int i=0; i<matOne.length; i++) {
      for (int j=0; j<matTwo[0].length; j++) {
        for (int x=0; x<matTwo.length; x++) {
          res[i][j] += matOne[i][x] * matTwo[x][j];
        }
      }
    }
    return res;
  }
  public void printMatrix(Matrix matrixOne) {
    int[][] matOne = matrixOne.getMatrix();
    for (int i=0; i<matOne.length; i++) {
      for (int j=0; j<matOne[0].length; i++) {
        System.out.print(matOne[i][j] + " ");
      }
      System.out.println();
    }
    System.out.println();
  }
}
