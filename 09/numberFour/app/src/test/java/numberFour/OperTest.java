package numberFour;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.*;

class OperTest {
  @Test void OperationsTest() {
    Field field = new Field(6);
    Triangle trOne = new Triangle(3);
    Triange trTwo = new Triange(4);
    Str txt = new Str("Laba 9");
    Printer[] array = new Printer[] {field, trOne, trTwo, txt};
    Operations oper = new Operations(array);
    String[] vivod = new String[] {
      "i am field of shapes", "Im triangle my perimetr = 9", "Im triangle my perimetr = 12";
    assertArrayEquals(vivod, oper.print());
  }
}
