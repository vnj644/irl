package numberFour;

public class Main {
  public static void main(String[] args) {
    Printer[] array = new Printer[5];
    array[0] = new Triangle(5);
    array[1] = new Triangle(3);
    array[2] = new Triangle(9);
    array[3] = new Str("laba 9");
    array[4] = new Str("point 4");
    Operations oper = new Operations(array);
    oper.printField();
  }
}
