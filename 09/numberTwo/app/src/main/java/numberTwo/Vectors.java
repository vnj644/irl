package numberTwo;

public class Vectors {
  private double x1;
  private double y1;
  private double x2;
  private double y2;
  private double skalarX;
  private double skalarY;
  Vectors(double x1, double y1, double x2, double y2) {
    this.x1 = x1;
    this.y1 = y1;
    this.x2 = x2;
    this.y2 = y2;
  }
  public double getXFirst() {
    return this.x1;
  }
  public void setXFirst(double x1) {
    this.x1 = x1;
  }
  public double getYFirst() {
    return this.y1;
  }
  public void setYFirst(double y1) {
    this.y1 = y1;
  }
  public double getXSecond() {
    return this.x2;
  }
  public void setXSecond(double x2) {
    this.x2 = x2;
  }
  public double getYSecond() {
    return this.y2;
  }
  public void setYSecond(double y2) {
    this.y2 = y2;
  }
  public double getSkalarX() {
    return this.x2 - this.x1;
  }
  public double getSkalarY() {
    return this.y2 - this.y1;
  }
  public String toString() {
    return this.x1 + " " + this.y1 + " " + this.x2 + " " + this.y2;
  }
  public static Vectors still(Vectors vetOne, Vectors vetTwo) {
    Vectors res = new Vectors(vetOne.getXFirst(), vetOne.getYFirst(), vetOne.getXSecond() + vetTwo.getSkalarX(), vetOne.getYSecond() + vetTwo.getSkalarY());
    return res;
  }
  public static Vectors subtract(Vectors vetOne, Vectors vetTwo) {
    Vectors res = new Vectors(vetOne.getXFirst(), vetOne.getYFirst(), vetOne.getXSecond() - vetTwo.getSkalarX(), vetOne.getYSecond() - vetTwo.getSkalarY());
    return res;
  }
  public Vectors multiplySkalar(double skalar) {
    Vectors res = new Vectors(this.getXFirst(), this.getYFirst(),this.getXSecond()*skalar, this.getYSecond()*skalar);
    return res;
  }
  public Vectors Skalar(double skalar) {
    Vectors res = new Vectors(this.getXFirst(), this.getYFirst(), this.getXSecond()/skalar, this.getYSecond()/skalar);
    return res;
  }

}
