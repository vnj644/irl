package numberTwo;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.*;

class VectorsTest {
  Vectors vetOne;
  Vectors vetTwo;
  Vectors vetThree;
  @BeforeEach
  public void make() {
    vetOne = new Vectors(1, 3, 8, 12);
    vetTwo = new Vectors(5, 2, 9, 14);
    vetThree = new Vectors(10, 3, 7, 20);
  }
  @Test void toStr() {
    assertEquals("1.0 3.0 8.0 12.0", vetOne.toString());
  }
  @Test void Skal() {
    assertEquals(-7.0, vetOne.getSkalarX());
    assertEquals(-12.0, vetOne.getSkalarY());
    assertEquals(1.0, vetTwo.getSkalarX());
    assertEquals(5.0, vetTwo.getSkalarY());
  }
  @Test void stilling() {
    Vectors createVet = Vectors.still(vetOne, vetTwo);
    assertEquals("1.0 3.0 1.0 5.0", createVet.toString());
  }
  @Test void multiplySkalar() {
    Vectors createVet = vetThree.multiplySkalar(2);
    assertEquals("10.0 3.0 7.0 20.0", createVet.toString());
  }
  @Test void skalar() {
    Vectors createVet = vetThree.skalar(5);
    assertEquals("10.0 3.0 70.0 200.0", createVet.toString());
  }
}
